'''
@file: conftest.py.py
@author: superhin
@date : 2021/9/12
'''
# 确保项目根目录被导入，主要编写钩子方法
import os

import platform
from configparser import ConfigParser

import pytest

from utils.send_email import send_email
from datetime import datetime
from py.xml import html


def pytest_configure(config):
    """将日志配置的生成绝对路径输出到reports目录"""
    rootdir = config.rootdir  # 项目根目录
    log_file_name = config.getini('log_file')
    log_file = os.path.join(rootdir, 'reports', log_file_name)  # 组装绝对路径
    config.option.log_file = log_file   # 修改命令行选项，相当于命令行加了一个--log-file=日志绝对路径

    metadata = config._metadata
    cur_env = config.getoption('--env') or config.getini('env')
    if cur_env:
        metadata["测试环境"] = cur_env

    tester = config.getoption('--tester') or config.getini('tester')
    if cur_env:
        metadata["测试人"] = tester

    # 移除替换环境变量
    java_home = metadata.pop('JAVA_HOME')
    packages = metadata.pop('Packages')
    platform = metadata.pop('Platform')
    plugins = metadata.pop('Plugins')
    python = metadata.pop('Python')
    metadata['测试平台'] = platform


def pytest_html_results_table_header(cells):
    cells.insert(2, html.th("Start Time", class_="sortable time", col="time"))


def pytest_html_results_table_row(report, cells):
    cells.insert(2, html.td(datetime.utcnow(), class_="col-time"))


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    report = outcome.get_result()
    setattr(report, "duration_formatter", "%H:%M:%S.%f")


def pytest_terminal_summary(terminalreporter, exitstatus, config):
    """"""
    # 执行完用例根据参数自动发送邮件
    body = config.getini('email_body')
    subject = config.getini('email_subject')
    receivers = config.getini('email_receivers').split(',')
    receivers = [item.strip() for item in receivers]
    smtp_server = config.getini('smtp_server')
    smtp_user = config.getini('smtp_user')
    smtp_password = config.getini('smtp_password')

    if config.getoption('--send_email') or config.getini('send_email'):
        send_email(smtp_server, smtp_user, smtp_password,
                   body=body,
                   subject=subject,
                   attachments=[config.option.log_file],
                   receivers=receivers)

    # 将allure数据生成html报告
    allure_command_line_dir = os.path.join(config.rootdir, 'tools', 'allure-2.12.0')
    if 'Windows' in platform.platform():
        allure_bin = os.path.join(allure_command_line_dir, 'bin', 'allure.bat')
    else:
        allure_bin = os.path.join(allure_command_line_dir, 'bin', 'allure')

    allure_data = config.getoption('--alluredir')
    allure_generate = config.getini('allure_generate')
    if allure_data and allure_generate:
        allure_html = os.path.join(config.rootdir, 'reports', 'allure_html')  # todo 配置
        cmd = '%s generate %s -o %s --clean' % (allure_bin, allure_data, allure_html)
        os.system(cmd)
        # cmd = '%s open %s' % (allure_bin, allure_html)
        os.system(cmd)


def pytest_addoption(parser):
    parser.addoption('--tester', action="store", help='test executor')
    parser.addini('tester', help='test executor')

    parser.addoption('--send_email', action="store_true", help='auto send email or not')
    parser.addini('send_email', help='auto send email or not')

    parser.addini('email_body', help='test report email body')
    parser.addini('email_subject', help='test report email subject')
    parser.addini('email_receivers', help='test report email receivers')

    parser.addini('smtp_server', help='smtp server')
    parser.addini('smtp_user', help='smtp user')
    parser.addini('smtp_password', help='smtp password')

    parser.addini('username', help='crm username')
    parser.addini('password', help='crm password')

    parser.addoption("--env", action="store", help="choose env: test,beta,prod")
    parser.addini('env', help="choose env: test,beta,prod")

    parser.addini('allure_generate', help="allure generate html or not")


def pytest_collection_modifyitems(items):
    """测试用例收集完成时，将收集到的item的name和nodeid的中文显示在控制台上"""
    pass
    # for item in items:
    #     item.name = item.name.encode("utf-8").decode("unicode_escape")
    #     item._nodeid = item.nodeid.encode("utf-8").decode("unicode_escape")


def pytest_html_report_title(report):
    """定制pytest-html测试报告标题"""
    report.title = "悟空CRM接口测试报告"


@pytest.fixture(scope='session')
def conf(request):
    """读取pytest.ini得到的ConfigParser对象"""
    pytest_ini = request.config.inifile
    conf = ConfigParser()
    conf.read(pytest_ini)
    return conf


@pytest.fixture(scope='session')
def env_vars(request, conf):
    """读取pytest.ini文件[global]和指定环境，如[prod]段变量"""
    config = request.config
    cur_env = config.getoption('--env') or config.getini('env')
    variables = {}
    if conf.has_section('global'):
        variables.update(conf.items('global'))
    if conf.has_section(cur_env):
        variables.update(conf.items(cur_env))
    return variables


@pytest.fixture(scope='session')
def base_url(env_vars):
    """读取pytest.ini对应环境段段base_url配置"""
    base_url = env_vars.get('base_url', '')
    if not base_url:
        pytest.skip('pytest.ini对应环境中未配置base_url')
    return base_url
