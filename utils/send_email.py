'''
@file: send_email.py
@author: superhin
@date : 2021/9/12
'''
import os
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import smtplib


def send_email(smtp_server, smtp_user, smtp_password,
               subject, body, attachments, receivers):
    """
    :param subject: 邮件主题
    :type subject: str
    :param body: 邮件正文，支持HTML
    :type body: str
    :param attachments: 附件路径列表
    :type attachments: list
    :param receivers: 收件人列表 ['superhinhan@tencent.com', 'superhin@126.com']
    :type receivers: list
    :return: None
    :rtype: NoneType
    """
    # 1. 组装邮件格式MIMEMultipart
    # 1.1实例化多部分邮件消息对象
    msg = MIMEMultipart()
    # 1.2 组装邮件头
    msg['From'] = smtp_user  # 发件人
    msg['To'] = ','.join(receivers)  # 收件人 'superhinhan@tencent.com,superhin@126.com'
    msg['Subject'] = subject  # 主题

    # 1.3 组装并添加邮件正文
    msg.attach(MIMEText(body, 'html', 'utf-8'))

    # 1.4 根据路径列表，组装并添加附件
    for file_path in attachments:
        file_name = os.path.basename(file_path)  # 通过路径拿到文件的文件名
        att = MIMEText(open(file_path, 'rb').read(), 'base64', 'utf-8')
        att['Content-Type'] = 'application/octet-stream'
        att['Content-Disposition'] = 'attachment; filename=%s' % file_name
        msg.attach(att)

    # 2. 登录smtp服务器发邮件
    smtp = smtplib.SMTP_SSL(smtp_server)  # 实例化smtp对象
    smtp.login(smtp_user, smtp_password)  # 登录服务
    smtp.sendmail(smtp_user, receivers, msg.as_string())  # 发送邮件
    print('邮件发送成功')


if __name__ == '__main__':
    send_email('测试', '<h2>正文</h2>',
               ['/Users/superhin/项目/ApiAuto/reports/log.txt'],
               ['superhin@126.com'])
