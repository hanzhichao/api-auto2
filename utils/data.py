"""数据文件读取"""
import os
import json
import csv
import logging

import yaml

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
DATA_DIR = os.path.join(BASE_DIR, 'data')


class Data:
    def __init__(self, data_dir=None):
        self.data_dir = data_dir or DATA_DIR

    def load_json(self, data_file):
        if self.data_dir:
            data_file = os.path.join(self.data_dir, data_file)

        with open(data_file, encoding='utf-8') as f:
            data = json.load(f)
        logging.info('读取JSON文件 %s 数据条数 %s' % (data_file, len(data)))
        return data

    def load_yaml(self, data_file):
        if self.data_dir:
            data_file = os.path.join(self.data_dir, data_file)

        with open(data_file, encoding='utf-8') as f:
            data = yaml.safe_load(f)
        logging.info('读取YAML文件 %s 数据条数 %s' % (data_file, len(data)))
        return data

    def reader_csv(self, data_file):
        if self.data_dir:
            data_file = os.path.join(self.data_dir, data_file)

        with open(data_file, encoding='utf-8') as f:
            reader = csv.DictReader(f)
            data = list(reader)
        logging.info('读取CSV文件 %s 数据条数 %s' % (data_file, len(data)))
        return data


data = Data()
