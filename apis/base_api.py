'''
@file: base_api.py
@author: superhin
@date : 2021/9/12
'''
from allure import step

from utils.api import HTTP


class BaseApi(HTTP):
    def __init__(self, username, password, base_url=None):
        self.username = username
        self.password = password
        super().__init__(base_url)
        self.auth()

    def auth(self):
        """登录并返回token"""
        with step('用户 %s 登录' % self.username):
            url = '/api/login'
            j = {
                "username": self.username,
                "password": self.password
            }

            res_dict = self.post(url, json=j)
            token = res_dict['data']['adminToken']
            self.session.headers = {"Admin-Token": token}

