'''
@file: customer.py.py
@author: superhin
@date : 2021/9/12
'''
"""客户模块相关接口"""
from allure import step

from apis.base_api import BaseApi


class Customer(BaseApi):
    """用户模块接口封装"""

    @step('添加客户')
    def add(self, entity):
        """发送添加客户请求，并返回字典格式都响应体数据"""
        url = '/api/crmCustomer/add'
        j = {
            "entity": entity,
            "field": []
        }
        res_dict = self.post(url, json=j)
        return res_dict

    @step('搜索客户')
    def search(self, name, page=1, limit=1000):
        url = '/api/crmCustomer/queryPageList'
        playload = {"page": page, "limit": limit, "search": name, "type":2, "sceneId": 37943}
        res_dict = self.post(url, json=playload)
        return res_dict

    @step('检查用户是否存在')
    def exist(self, name):
        res_dict = self.search(name)
        if len(res_dict['data']['list']) > 0:
            return True
        return False

    @step('批量删除客户')
    def batch_del(self, ids):
        url = '/api/crmCustomer/deleteByIds'
        playload = ids
        res_dict = self.post(url, json=playload)
        return res_dict

    @step('搜索并删除')
    def del_by_name(self, name):
        res_dict = self.search(name)
        ids = [item['customerId'] for item in res_dict['data']['list']]
        self.batch_del(ids)
        return res_dict


if __name__ == '__main__':
    c = Customer('17682366635', 'zjw123', 'https://www.72crm.com')
    print(c.add(entity={}))
