import pytest
from allure import feature, story


@feature('添加线索')
@story('正常添加线索')
def test_add_leads(session, data):
    url = 'https://www.72crm.com/api/crmLeads/add'
    json_data = data['leads1']
    res = session.post(url, json=json_data)
    res_dict = res.json()
    code = res_dict['code']
    assert code == 0

