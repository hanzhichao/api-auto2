import pytest
from allure import feature, story


@pytest.mark.skip('待迁移')
def test_add_customer01(session, data):
    url = 'https://www.72crm.com/api/crmCustomer/add'
    json_data = data['customer1']
    res = session.post(url, json=json_data)
    res_dict = res.json()
    code = res_dict['code']
    customer_name = res_dict['data']['customerName']
    assert code == 0
    assert customer_name == '临渊'


@pytest.mark.skip('待迁移')
def test_add_customer02(session, data):
    url = 'https://www.72crm.com/api/crmCustomer/add'
    json_data = data['customer2']
    res = session.post(url, json=json_data)
    res_dict = res.json()
    code = res_dict['code']
    customer_name = res_dict['data']['customerName']
    assert code == 0
    assert customer_name == '临渊2'


@feature('搜索客户')
@story('搜索存在的客户')
def test_search_customer_exists(customer):
    """测试搜索存在的客户"""
    res_dict = customer.search('临渊')
    assert res_dict['code'] == 0


@feature('搜索客户')
@story('搜索存不在的客户')
def test_search_customer_not_exists(customer):
    """测试搜索存不在的客户"""
    res_dict = customer.search('临渊羡鱼')
    assert res_dict['code'] == 0


@feature('删除客户')
@story('删除存在的客户')
def test_del_customer_exists(customer):
    """测试删除存在的客户"""
    customer_name = '临渊'
    if customer.exist(customer_name) is False:
        customer.add(entity={'customerName': customer_name})
    res_dict = customer.del_by_name(customer_name)
    assert res_dict['code'] == 0


@feature('删除客户')
@story('删除存不在的客户')
def test_del_customer_not_exists(customer):
    """测试搜索存不在的客户"""
    res_dict = customer.del_by_name('临渊羡鱼')
    assert res_dict['code'] == 0

